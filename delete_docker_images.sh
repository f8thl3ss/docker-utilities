#! /bin/bash
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
for img in $(docker images -a | awk '{print $3}' | tail -n +2); do docker rmi -f $img; done
